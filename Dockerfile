FROM node:17-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY server.js .

#For docker-compose
#ENV FRONTEND_SERVICE="frontend_app_1"
#ENV PRODUCTS_SERVICE="products_app_1"

#For kubernetes
ENV FRONTEND_SERVICE="frontend"
ENV PRODUCTS_SERVICE="products"

EXPOSE 3002
CMD ["npm", "start"]
